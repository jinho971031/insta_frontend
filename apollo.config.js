// apollo에게 백엔드의 존재를 알려준다.
module.exports = {
  client: {
    // ./src폴더의 모든 파일에서, 확장자가 .tsx나 .ts로 끝나는 파일을 선택한다.
    includes: ["./src/**/*.{tsx,ts}"],
    // 위에서 선택된 파일에 있는, 모든 gql 태그를 찾는다.
    tagName: "gql",
    service: {
      name: "instaclone-backend",
      url: "http://localhost:4000/graphql",
    },
  },
};
