/// <reference types="react-scripts" />

//--------------------------------------------------Props

interface Props {
  children: React.ReactNode;
}

// interface ButtonProps {
//   type: string;
//   value: string;
// }

interface InputProps {
  type: string;
  placeholder: string;
  hasError?: boolean;
}

interface BottomBoxProps {
  cta: string;
  link: string;
  linkText: string;
  fontWeight?: string;
}

interface AvatarProps {
  url?: any;
  size?: string;
}

interface PhotoLikeProps {
  likeCount: number;
  id: number;
}

interface ModalProps {
  isModalOn?: boolean;
  isMenuOn?: boolean;
}

interface ActionProps {
  isHover?: boolean;
}

interface ButtonProps {
  suggest?: boolean;
}

//--------------------------------------------------Components

interface SpanText {
  text: string;
}

interface PageTitle {
  title: string;
}

interface FatTextProps {
  color?: string;
  fontWeight?: string;
}

interface FormError {
  message?: string;
}

interface Notification {
  message?: [string, undefined];
}

//--------------------------------------------------Forms

interface LoginForm {
  username: string;
  password: string;
}

interface SignUpForm {
  email: string;
  name: string;
  username: string;
  password: string;
}

//--------------------------------------------------Results

interface LoginResult {
  login: {
    __typename: "LoginResult";
    ok: boolean;
    error?: string;
    token?: string;
  };
}

interface SignUpResult {
  createAccount: {
    ok: boolean;
    error?: string;
  };
}

//--------------------------------------------------Uses

interface useLocation {
  message: string;
  username: string;
  password: string;
}

//--------------------------------------------------Query
