import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { FatText } from "./shared";

const SCaption = styled.span`
  mark {
    background-color: inherit;
    color: ${(props) => props.theme.blue};
    cursor: pointer;
  }
  line-height: 20px;
`;

const SHLink = styled(Link)`
  color: ${(props) => props.theme.blue};
`;

const SULink = styled(Link)`
  color: ${(props) => props.theme.darkBlue};
`;

const SUsername = styled(FatText)`
  cursor: pointer;
  margin-right: 7px;
`;

function Caption({ caption, username }: any) {
  const splitWords = caption.split(" ").map((word: string, index: any) =>
    /#[\w]+/g.test(word) ? (
      <React.Fragment key={index}>
        <SHLink to={`/hashtags/${word}`}>{word}</SHLink>{" "}
      </React.Fragment>
    ) : /@[\w]+/g.test(word) ? (
      <React.Fragment key={index}>
        <SULink to={`/profile/${word}`}>{word}</SULink>{" "}
      </React.Fragment>
    ) : (
      <React.Fragment key={index}>{word} </React.Fragment>
    )
  );
  return (
    <SCaption>
      <Link to={`/users/${username}`}>
        <SUsername>{username}</SUsername>
      </Link>
      {splitWords}
    </SCaption>
  );
}

export default Caption;
