import { faUser } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

interface Img {
  size?: string;
}

const SAvatar = styled.div``;

const SImg = styled.img<Img>`
  width: ${(props) =>
    props.size
      ? props.size === "lg"
        ? "30px"
        : props.size === "sm" && "10px"
      : "20px"};
  height: ${(props) =>
    props.size
      ? props.size === "lg"
        ? "30px"
        : props.size === "sm" && "10px"
      : "20px"};
  border: 1px solid gray;
  border-radius: 50%;
`;

const AvatarBox = styled.div`
  width: 30px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid gray;
  border-radius: 50%;
  background-color: ${(props) => props.theme.borderColor};
`;

function Avatar({ url = "", size }: AvatarProps) {
  return (
    <SAvatar>
      {url !== "" && url !== null ? (
        <SImg src={url} alt="avatar" size={size} />
      ) : (
        <AvatarBox>
          <FontAwesomeIcon icon={faUser} size="lg" />
        </AvatarBox>
      )}
    </SAvatar>
  );
}

export default Avatar;
