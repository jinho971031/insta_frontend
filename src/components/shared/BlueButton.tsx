import styled from "styled-components";

const SButton = styled.div`
  width: 100px;
  height: 50px;
  background-color: ${(props) => props.theme.blue};
`;

const BlueButton = () => {
  return <SButton></SButton>;
};

export default BlueButton;
