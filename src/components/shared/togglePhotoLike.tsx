import { TOGGLE_PHOTO_LIKE_MUTATION } from "../../gql/mutations";
import { gql, useMutation } from "@apollo/client";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faHeart as solidHeart } from "@fortawesome/free-solid-svg-icons";

const PhotoAction = styled.div`
  :not(:last-child) {
    margin-right: 15px;
  }
  cursor: pointer;
`;

const TogglePhotoLike = ({ id, isLiked, likeCount }: any) => {
  //----------------[ 좋아요 업데이트 관리 ]--------------------//
  const updatePhotoLike = (cache: any, result: any) => {
    const {
      data: {
        togglePhotoLike: { ok },
      },
    } = result;

    if (ok) {
      const fragmentId = `Photo:${id}`;
      const fragment = gql`
        fragment BSName on Photo {
          isLiked
          likeCount
        }
      `;
      const result = cache.readFragment({
        id: fragmentId,
        fragment,
      });

      if ("isLiked" in result && "likeCount" in result) {
        const { isLiked: cacheIsLiked, likeCount: cacheLikeCount } = result;
        cache.writeFragment({
          id: fragmentId,
          fragment,
          data: {
            isLiked: !cacheIsLiked,
            likeCount: cacheIsLiked ? cacheLikeCount - 1 : cacheLikeCount + 1,
          },
        });
      }
    }
  };

  //----------------[ 좋아요 DB 관리 ]--------------------//
  const [PhotoLike] = useMutation(TOGGLE_PHOTO_LIKE_MUTATION, {
    variables: {
      photoId: id,
    },
    update: updatePhotoLike,
    // refetchQueries: [{ query: FEED_QUERY }],
  });

  return (
    <PhotoAction onClick={() => PhotoLike()}>
      <FontAwesomeIcon
        icon={isLiked ? solidHeart : faHeart}
        size="2x"
        style={{ color: isLiked ? "tomato" : "inherit" }}
      />
    </PhotoAction>
  );
};

export default TogglePhotoLike;
