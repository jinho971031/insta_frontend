import { useReactiveVar } from "@apollo/client";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faCompass } from "@fortawesome/free-regular-svg-icons";
import {
  faHome,
  faSearch,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";
import { hasTokenVar } from "../../apollo";
import routes from "../../routes";
import ModalProfile from "../modal/ModalProfile";
import useUser from "../../hooks/useUser";
import Avatar from "../shared/Avatar";
import DarkModeButton from "./DarkModeButton";
import { useState } from "react";
import { useForm } from "react-hook-form";

const Container = styled.header`
  width: 100%;
  border: 0.5px solid ${(props) => props.theme.borderColor};
  background-color: ${(props) => props.theme.boxColor};
  padding: 10px 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  z-index: 50;
  user-select: none;
`;

const Wrapper = styled.div`
  max-width: 930px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Divider = styled.div`
  width: 33%;
  display: flex;
  justify-content: center;
  :first-child {
    justify-content: flex-start;
  }
  :last-child {
    justify-content: flex-end;
  }
`;

const ToHome = styled(Link)`
  cursor: pointer;
  text-decoration: none;
  color: ${(props) => props.theme.fontColor};
`;

const SearchBar = styled.div`
  width: 200px;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 0.5px solid ${(props) => props.theme.borderColor};
  border-radius: 5px;
  background-color: ${(props) => props.theme.bgColor};
  text-align: center;
  position: relative;
  z-index: 98;
`;

const IconBox = styled.div`
  width: 100%;
  display: none;
  justify-content: space-between;
  position: absolute;
  padding: 0 5px;
  opacity: 0.5;
`;

const Time = styled.div`
  cursor: pointer;
  z-index: 99;
`;
const Search = styled.div`
  opacity: 0.5;
  cursor: pointer;
  z-index: 99;
  padding: 0 5px;
`;

const SearchForm = styled.form`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;

const SearchInput = styled.input`
  width: auto;
  overflow: hidden;
  cursor: text;
  opacity: 0.5;

  :focus {
    width: 85%;
    text-align: start;
  }

  :focus ~ ${Search} {
    display: none;
  }
`;

const NavBox = styled.div`
  display: flex;
`;

const IconBar = styled.div`
  display: flex;
  align-items: center;
`;
const Icon = styled.div`
  position: relative;
  cursor: pointer;
  margin-right: 20px;
`;

const Button = styled.span`
  background-color: ${(props) => props.theme.blue};
  border-radius: 5px;
  padding: 5px 15px;
  color: white;
  font-weight: 600;
`;

function Header() {
  //로컬스토리지에 토큰 가지고 있는지 확인하기
  const hasToken = useReactiveVar(hasTokenVar);
  const { data }: any = useUser();
  const history = useHistory();

  const [onProfile, setOnProfile] = useState(false);

  const { register, handleSubmit, getValues } = useForm();

  let currentState = false;

  const clickBodyEvent = (e: any) => {
    const target = e.target;

    if (target !== currentState) {
      // 메뉴 끄기
      setOnProfile(false);
      // body 이벤트 지우기
      document.body.removeEventListener("click", clickBodyEvent);
      return;
    }
  };

  const handleClick = (e: any) => {
    // 초기 상황값 (아바타 이미지) 설정

    if (!currentState) {
      currentState = e.target;
    }

    // 설정창 켜기
    setOnProfile(true);

    // 바디에 이벤트 리스너 추가
    document.body.addEventListener("click", clickBodyEvent);
  };

  const onSubmit = () => {
    const result = getValues();
    const { search } = result;
    history.push(`/users/${search}`);
  };

  return (
    <Container>
      <Wrapper>
        <Divider>
          <ToHome to={routes.home}>
            <FontAwesomeIcon icon={faInstagram} size="2x" />
          </ToHome>
        </Divider>
        <Divider>
          <SearchBar className="search">
            <SearchForm onSubmit={handleSubmit(onSubmit)}>
              <Search>
                <FontAwesomeIcon icon={faSearch} size="sm" />
              </Search>
              <SearchInput
                {...register("search")}
                type="text"
                placeholder="검색"
                autoCapitalize="none"
                role="textbox"
              />
            </SearchForm>

            <IconBox>
              <Search>
                <FontAwesomeIcon icon={faSearch} size="sm" />
              </Search>
              <Time>
                <FontAwesomeIcon icon={faTimesCircle} size="sm" />
              </Time>
            </IconBox>
          </SearchBar>
        </Divider>
        <Divider>
          <NavBox>
            <DarkModeButton />
            {hasToken ? (
              <IconBar>
                <Icon>
                  <FontAwesomeIcon icon={faHome} size="lg" />
                </Icon>
                <Icon>
                  <FontAwesomeIcon icon={faCompass} size="lg" />
                </Icon>
                <Icon>
                  <div onClick={handleClick}>
                    <Avatar url={data?.me?.avatar} />
                  </div>
                  {onProfile ? <ModalProfile /> : null}
                </Icon>
              </IconBar>
            ) : (
              <Link to={routes.home}>
                <Button>Login</Button>
              </Link>
            )}
          </NavBox>
        </Divider>
      </Wrapper>
    </Container>
  );
}

export default Header;
