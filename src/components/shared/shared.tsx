import styled from "styled-components";

export const BaseBox = styled.div`
  background-color: ${(props) => props.theme.boxColor};
  color: ${(props) => props.theme.fontColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  border-radius: 2px;
  width: 100%;
`;

export const FatLink = styled.span`
  font-weight: 600;
  color: "rgb(142,142,142)";
`;

export const FatText = styled.span<FatTextProps>`
  color: ${(props) => (props.color ? props.color : "inherits")};
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : 700)};
`;

export const BlueButton = styled.button`
  all: unset;
  background-color: ${(props) => props.theme.blue};
  color: ${(props) => props.theme.white};
  padding: 7px 10px;
  font-weight: 600;
  outline: none;
  border-radius: 5px;
  :not(:last-child) {
    margin-right: 10px;
  }
  cursor: pointer;
  :active {
    opacity: 0.5;
  }
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
`;
