import { Helmet } from "react-helmet-async";

function PageTitle({ title }: PageTitle) {
  return (
    <Helmet>
      <title>{title} | Instagram</title>
    </Helmet>
  );
}

export default PageTitle;
