import { useReactiveVar } from "@apollo/client";
import { faMoon, faSun } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { DarkMode, darkModeVar, LiteMode } from "../../apollo";

const ButtonLayout = styled.div`
  display: flex;
  user-select: none;
  border-radius: 5px;
  cursor: pointer;
  margin: 0 15px;
`;

function DarkModeButton() {
  const darkMode = useReactiveVar(darkModeVar);

  return (
    <ButtonLayout onClick={darkMode ? LiteMode : DarkMode}>
      <FontAwesomeIcon icon={darkMode ? faSun : faMoon} size="lg" />
    </ButtonLayout>
  );
}

export default DarkModeButton;
