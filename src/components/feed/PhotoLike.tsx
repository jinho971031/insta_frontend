import { useQuery } from "@apollo/client";
import styled from "styled-components";
import { SEE_PHOTO_LIKES_QUERY } from "../../gql/queries";
import Avatar from "../shared/Avatar";
import { FatText } from "../shared/shared";

const Likes = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 12px;
  span {
    margin-left: 5px;
  }
`;

function PhotoLike({ likeCount, id }: PhotoLikeProps) {
  const { data } = useQuery(SEE_PHOTO_LIKES_QUERY, {
    variables: {
      photoId: id,
    },
  });

  if (likeCount === 1) {
    return (
      <Likes>
        <Avatar url={data?.seePhotoLikes[0]?.avatar} />
        <FatText>{data?.seePhotoLikes[0]?.username}</FatText>님이 좋아합니다
      </Likes>
    );
  }
  if (likeCount > 1) {
    return (
      <Likes>
        <Avatar url={data?.seePhotoLikes[0]?.avatar} />
        <FatText>{data?.seePhotoLikes[0]?.username}</FatText>님{" "}
        <FatText>외 {likeCount}명</FatText>이 좋아합니다
      </Likes>
    );
  } else {
    return (
      <Likes>
        <FatText>좋아요 {likeCount}개</FatText>
      </Likes>
    );
  }
}

export default PhotoLike;
