import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import Caption from "../shared/Caption";

const CommentContainer = styled.div`
  margin-top: 10px;
`;

const CommentLink = styled.span`
  display: block;
  opacity: 0.5;
  cursor: pointer;
  margin-bottom: 10px;
`;

const Comment = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;

const Info = styled.div`
  display: flex;
`;

function Comments({ comments, commentCount, openModal }: any): any {
  if (!comments || !commentCount) {
    return null;
  }

  if (comments.length > 2) {
    comments = comments.slice(-2);
  }

  return (
    <CommentContainer>
      {commentCount > 2 && (
        <CommentLink onClick={openModal}>
          댓글 {commentCount}개 모두 보기
        </CommentLink>
      )}
      {comments.map((comment: any) => (
        <Comment key={comment?.id}>
          <Info>
            <Caption
              caption={comment?.comment}
              username={comment?.user.username}
            />
          </Info>
          <FontAwesomeIcon icon={faHeart} size="sm" />
        </Comment>
      ))}
    </CommentContainer>
  );
}

export default Comments;
