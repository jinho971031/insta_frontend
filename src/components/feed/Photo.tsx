import { useState } from "react";
import styled from "styled-components";
import {
  faBookmark,
  faComment,
  faPaperPlane,
} from "@fortawesome/free-regular-svg-icons";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PhotoLike from "./PhotoLike";
import Comments from "./Comments";
import CreateComment from "./CreateComment";
import ModalPhoto from "../modal/ModalPhoto";
import { onModal, offModal } from "../../apollo";
import { FatText } from "../shared/shared";
import Avatar from "../shared/Avatar";
import Caption from "../shared/Caption";
import TogglePhotoLike from "../shared/togglePhotoLike";
import { Link } from "react-router-dom";

const SPhoto = styled.div`
  background-color: ${(props) => props.theme.boxColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  margin-bottom: 25px;
`;

const User = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 0.5px solid ${(props) => props.theme.borderColor};
  padding: 15px;
`;

const ToUser = styled(Link)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  cursor: pointer;
  span {
    margin-left: 10px;
  }
`;
const RightBox = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

//-----------------------------------------

const Images = styled.div`
  width: 100%;
  user-select: none;
`;
const Img = styled.img`
  width: 100%;
`;

//-----------------------------------------

const Contents = styled.div`
  border-top: ${(props) => props.theme.borderColor};
`;

const Top = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px 17px;
`;

const LeftBar = styled.div`
  width: 25%;
  display: flex;
  justify-content: flex-start;
  flex-grow: 1;
`;

const CenterBar = styled.div`
  display: flex;
  justify-content: center;
  flex-grow: 2;
`;

const RightBar = styled.div`
  width: 25%;
  display: flex;
  justify-content: flex-end;
  flex-grow: 1;
`;

const PhotoAction = styled.div`
  :not(:last-child) {
    margin-right: 15px;
  }
  cursor: pointer;
`;

//-----------------------------------------

const Middle = styled.div`
  border-bottom: 0.5px solid ${(props) => props.theme.borderColor};
  padding: 0 17px 10px 17px;
  user-select: none;
`;

const CreateAt = styled.div`
  font-size: 10px;
  opacity: 0.5;
  padding: 3px 0;
`;

//-----------------------------------------

function Photo({
  id,
  user,
  file,
  isLiked,
  likeCount,
  comments,
  commentCount,
  caption,
  createdAt,
}: any) {
  //----------------[ 모달 관리 ]--------------------//
  const [isModalOn, setOnModal] = useState(false);

  const openModal = () => {
    setOnModal(true);
    console.log("Modal opened.");
    onModal();
  };

  const closeModal = () => {
    setOnModal(false);
    console.log("Modal colsed.");
    offModal();
  };

  return (
    <SPhoto key={id}>
      <User>
        <ToUser to={`/users/${user.username}`}>
          <Avatar url={user.avatar} size="lg" />
          <FatText>{user.username}</FatText>
        </ToUser>
        <RightBox>
          <FontAwesomeIcon icon={faEllipsisH} />
        </RightBox>
      </User>
      <Images>
        <Img src={file} alt="file" />
      </Images>
      <Contents>
        <Top>
          <LeftBar>
            <TogglePhotoLike id={id} isLiked={isLiked} likeCount={likeCount} />
            <PhotoAction>
              <FontAwesomeIcon icon={faComment} size="2x" />
            </PhotoAction>
            <PhotoAction>
              <FontAwesomeIcon icon={faPaperPlane} size="2x" />
            </PhotoAction>
          </LeftBar>
          <CenterBar>
            <FontAwesomeIcon icon={faEllipsisH} size="2x" />
          </CenterBar>
          <RightBar>
            <PhotoAction>
              <FontAwesomeIcon icon={faBookmark} size="2x" />
            </PhotoAction>
          </RightBar>
        </Top>
        <Middle>
          {likeCount > 0 ? <PhotoLike likeCount={likeCount} id={id} /> : null}
          <Caption caption={caption} username={user.username} key={id} />
          <Comments
            photoId={id}
            user={user}
            comments={comments}
            commentCount={commentCount}
            createdAt={createdAt}
            openModal={openModal}
          />
          <ModalPhoto
            isModalOn={isModalOn}
            closeModal={closeModal}
            id={id}
            file={file}
            user={user}
            caption={caption}
            comments={comments}
            isLiked={isLiked}
            likeCount={likeCount}
            createdAt={createdAt}
          />
          <CreateAt>{createdAt}</CreateAt>
        </Middle>
        <CreateComment
          type="text"
          placeholder="댓글 달기..."
          photoId={id}
          comments={comments}
        />
      </Contents>
    </SPhoto>
  );
}

export default Photo;
