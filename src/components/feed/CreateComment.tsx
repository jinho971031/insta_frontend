import { gql, useMutation } from "@apollo/client";
import { faSmile } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useForm } from "react-hook-form";
import styled from "styled-components";
import { CREATE_COMMENT_MUTATION } from "../../gql/mutations";
import useUser from "../../hooks/useUser";

const Bottom = styled.div`
  display: flex;
  width: 100%;
  padding: 15px 17px;
  form {
    width: 100%;
    display: flex;
    justify-content: space-between;
  }
`;
const Emoji = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 15px;
`;

const Create = styled.input`
  width: 100%;
`;

const Publish = styled.input`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 35px;
  margin-left: 10px;
  font-weight: 600;
  color: ${(props) => props.theme.blue};
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
`;

function CreateComment(props: any) {
  const { photoId, comments, ...rest } = props;

  const { data: userData } = useUser();

  const { register, formState, getValues, handleSubmit, setValue } = useForm({
    mode: "onChange",
  });

  const updateNewComment = (cache: any, result: any) => {
    const { comment } = getValues();
    setValue("comment", "");
    const {
      data: {
        createComment: { ok, id: commentId },
      },
    } = result;

    if (ok && userData?.me) {
      const newComment = {
        id: commentId,
        __typename: "Comment",
        user: {
          ...userData.me,
        },
        comment,
        isMine: true,
        createdAt: Date.now(),
      };

      const newCacheComment = cache.writeFragment({
        data: newComment,
        fragment: gql`
          fragment BSName on Comment {
            id
            user {
              username
              avatar
            }
            comment
            isMine
            createdAt
          }
        `,
      });

      cache.modify({
        id: `Photo:${photoId}`,
        fields: {
          comments(prev: any) {
            return [...prev, newCacheComment];
          },
          commentCount(prev: any) {
            return prev + 1;
          },
        },
      });
    }
  };

  const [createComment, { loading }] = useMutation(CREATE_COMMENT_MUTATION, {
    update: updateNewComment,
  });

  const onSubmit = () => {
    if (loading) {
      return;
    }
    const { comment } = getValues();
    createComment({ variables: { comment, photoId } });
  };

  return (
    <Bottom>
      <Emoji>
        <FontAwesomeIcon icon={faSmile} size="2x" />
      </Emoji>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Create {...register("comment", { required: true })} {...rest} />
        <Publish
          type="submit"
          value={loading ? "로딩중..." : "게시"}
          disabled={!formState.isValid || loading}
        />
      </form>
    </Bottom>
  );
}

export default CreateComment;
