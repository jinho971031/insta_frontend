import { useMutation } from "@apollo/client";
import { faUserCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { UNFOLLOW_MUTATION } from "../../gql/mutations";
import { BlueButton } from "../shared/shared";

const SButton = styled(BlueButton)`
  background-color: ${(props) => props.theme.boxColor};
  color: ${(props) => props.theme.fontColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  padding: 0 10px;
  height: 30px;
`;

const UnFollowButton = ({ username, myname }: any) => {
  const updateUnfollowState = (cache: any, result: any) => {
    const {
      data: {
        unfollow: { ok, error },
      },
    } = result;

    if (!ok) {
      console.log(error);
      return;
    }

    cache.modify({
      id: `User:${username}`,
      fields: {
        isFollowing() {
          return false;
        },
        totalFollowers(prev: any) {
          return prev - 1;
        },
      },
    });

    cache.modify({
      id: `User:${myname}`,
      fields: {
        totalFollowing(prev: any) {
          return prev - 1;
        },
      },
    });
  };

  const [unfollow, { loading }] = useMutation(UNFOLLOW_MUTATION, {
    variables: { username },
    update: updateUnfollowState,
  });

  const onSubmit = () => {
    const result = unfollow();
    console.log(result);
  };
  return (
    <SButton disabled={loading} onClick={onSubmit}>
      {loading ? "로딩중..." : <FontAwesomeIcon icon={faUserCheck} />}
    </SButton>
  );
};

export default UnFollowButton;
