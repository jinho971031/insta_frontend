import { useMutation } from "@apollo/client";
import styled from "styled-components";
import { FOLLOW_MUTATION } from "../../gql/mutations";
import { BlueButton } from "../shared/shared";

const SButton = styled(BlueButton)`
  padding: 0 25px;
  height: 30px;
`;

const FollowButton = ({ username, myname }: any) => {
  const updateFollowState = (cache: any, result: any) => {
    const {
      data: {
        follow: { ok, error },
      },
    } = result;

    if (!ok) {
      console.log(error);
      return;
    }

    cache.modify({
      id: `User:${username}`,
      fields: {
        isFollowing() {
          return true;
        },
        totalFollowers(prev: any) {
          return prev + 1;
        },
      },
    });

    cache.modify({
      id: `User:${myname}`,
      fields: {
        totalFollowing(prev: any) {
          return prev + 1;
        },
      },
    });
  };

  const [follow, { loading }] = useMutation(FOLLOW_MUTATION, {
    variables: { username },
    update: updateFollowState,
  });

  const onSubmit = () => {
    const result = follow();
    console.log(result);
  };

  return (
    <SButton disabled={loading} onClick={onSubmit}>
      {loading ? "로딩중..." : "팔로우"}
    </SButton>
  );
};

export default FollowButton;
