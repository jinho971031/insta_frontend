import { faComment, faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import styled from "styled-components";
import { offModal, onModal } from "../../apollo";
import ModalPhoto from "../modal/ModalPhoto";

const Container = styled.div``;
const PhotoBox = styled.div<ModalProps>`
  display: flex;
  align-items: center;
  width: 280px;
  height: 280px;
  overflow: hidden;
  margin: 15px;
  :hover {
    section {
      display: flex;
    }
  }
  position: relative;
`;

const Photo = styled.img`
  width: 100%;
  object-fit: cover;
`;

const Info = styled.section`
  display: none;
  justify-content: center;
  align-items: center;
  background-color: rgba(1, 1, 1, 0.5);
  width: 280px;
  height: 280px;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 3;
  cursor: pointer;
`;

const Icon = styled.div`
  :not(:last-child) {
    margin-right: 10px;
  }
  span {
    color: white;
    font-weight: 600;
    margin-left: 5px;
  }
`;

const ProfilePhoto = ({ photo, idx }: any) => {
  const [isModalOn, setOnModal] = useState(false);

  const openModal = () => {
    setOnModal(true);
    console.log("Modal opened.");
    onModal();
  };

  const closeModal = () => {
    setOnModal(false);
    console.log("Modal colsed.");
    offModal();
  };

  return (
    <Container key={idx}>
      <PhotoBox isModalOn={isModalOn}>
        <Photo src={photo.file} />
        <Info onClick={openModal}>
          <Icon>
            <FontAwesomeIcon icon={faHeart} color="white" size="lg" />
            <span>{photo.likeCount}</span>
          </Icon>
          <Icon>
            <FontAwesomeIcon icon={faComment} color="white" size="lg" />
            <span>{photo.commentCount}</span>
          </Icon>
        </Info>
      </PhotoBox>
      <ModalPhoto
        isModalOn={isModalOn}
        closeModal={closeModal}
        id={photo.id}
        file={photo.file}
        user={photo.user}
        caption={photo.caption}
        comments={photo.comments}
        isLiked={photo.isLiked}
        likeCount={photo.likeCount}
        createdAt={photo.createdAt}
      />
    </Container>
  );
};

export default ProfilePhoto;
