import styled from "styled-components";
import { faBookmark, faUser } from "@fortawesome/free-regular-svg-icons";
import {
  faAngleDown,
  faAngleUp,
  faCog,
  faEllipsisH,
  faTh,
  faUserTag,
  faVideo,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { BlueButton } from "../shared/shared";
import PageTitle from "../shared/PageTitle";
import FollowButton from "./FollowButton";
import Separator from "../auth/Separator";
import ProfilePhoto from "./ProfilePhoto";
import UnFollowButton from "./UnFollowButton";
import { useState } from "react";
import useUser from "../../hooks/useUser";

const Container = styled.div`
  padding-top: 50px;
`;
const Top = styled.div`
  display: flex;
`;
const Bottom = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const ImgBox = styled.div`
  width: 320px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
`;

const AvatarBox = styled.div`
  width: 150px;
  height: 150px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  background-color: ${(props) => props.theme.borderColor};
`;

const Img = styled.img`
  width: 200px;
  border-radius: 50%;
`;
const ContentBox = styled.div``;
const InfoBar = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 20px;
`;

const TopBar = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`;

const Box = styled.div`
  :not(:last-child) {
    margin-right: 10px;
  }
  span {
    font-size: 16px;
    :not(:last-child) {
      margin-right: 5px;
    }
    :last-child {
      margin-right: 20px;
      font-weight: 700;
    }
  }
`;

const ChangeProfileButton = styled.div`
  margin-left: 5px;
  cursor: pointer;
  :active {
    opacity: 0.5;
  }
`;

const Username = styled.div`
  font-size: 28px;
  margin-right: 20px;
`;
const Icon = styled.div`
  :not(:last-child) {
    margin-right: 50px;
  }
  font-size: 12px;
  font-weight: 600;
  span {
    margin-left: 5px;
  }
  :active {
    opacity: 0.5;
  }
`;

const Bio = styled.div`
  font-size: 16px;
`;
const Title = styled.div`
  font-weight: 600;
  margin-bottom: 10px;
`;

const SButton = styled.button`
  background-color: ${(props) => props.theme.boxColor};
  color: ${(props) => props.theme.fontColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  border-radius: 5px;
  padding: 0 10px;
  height: 30px;
  outline: none;
  font-weight: 600;
  :active {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const FollowCheck = styled.div`
  padding: 0 10px;
`;

const SuggestButton = styled(BlueButton)<ButtonProps>`
  background-color: ${(props) => props.theme.boxColor};
  color: ${(props) => props.theme.fontColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  opacity: ${(props) => (props.suggest ? 0.5 : 1)};
`;

const MoreButton = styled.div`
  cursor: pointer;
`;

//----------------------------------------------------------

const Story = styled.div`
  display: none;
  padding: 10px 0 10px 20px;
  margin: 20px 0 25px 20px;
`;

const Category = styled.div`
  margin-right: 10px;
`;

//---------------------------------------------------------

const BottomTop = styled.div`
  display: flex;
  margin-bottom: 20px;
`;
const Photos = styled.div`
  display: flex;
  justify-content: center;
`;

const ProfileDetail = (data: any) => {
  const {
    data: {
      name,
      username,
      avatar,
      isMe,
      isFollowing,
      totalPhotos,
      totalFollowers,
      totalFollowing,
      photos,
    },
  } = data;

  const {
    data: { me },
  } = useUser();

  const [suggest, setSuggest] = useState(false);

  return (
    <Container>
      <PageTitle title={name + "(@" + username + ")"} />
      <Top>
        <ImgBox>
          <AvatarBox>
            {avatar ? (
              <Img src={avatar} alt="profile" />
            ) : (
              <FontAwesomeIcon icon={faUser} size="4x" />
            )}
          </AvatarBox>
        </ImgBox>

        <ContentBox>
          {isMe ? (
            <TopBar>
              <Username>{username}</Username>

              <SButton>프로필 편집</SButton>
              <ChangeProfileButton>
                <FontAwesomeIcon icon={faCog} />
              </ChangeProfileButton>
            </TopBar>
          ) : (
            <TopBar>
              <Username>{username}</Username>
              <SButton>메시지 보내기</SButton>
              {isFollowing ? (
                <FollowCheck>
                  <UnFollowButton username={username} myname={me?.username} />
                </FollowCheck>
              ) : (
                <FollowCheck>
                  <FollowButton username={username} myname={me?.username} />
                </FollowCheck>
              )}
              <SuggestButton
                onClick={() => setSuggest(!suggest)}
                suggest={suggest}
              >
                {suggest ? (
                  <FontAwesomeIcon icon={faAngleUp} />
                ) : (
                  <FontAwesomeIcon icon={faAngleDown} />
                )}
              </SuggestButton>
              <MoreButton>
                <FontAwesomeIcon icon={faEllipsisH} />
              </MoreButton>
            </TopBar>
          )}
          <InfoBar>
            <Box>
              <span>게시물</span>
              <span>{totalPhotos}</span>
            </Box>
            <Box>
              <span>팔로워</span>
              <span>{totalFollowers}</span>
            </Box>
            <Box>
              <span>팔로잉</span>
              <span>{totalFollowing}</span>
            </Box>
          </InfoBar>
          <Bio>
            <Title>{name}</Title>
            <Box>설명</Box>
          </Bio>
        </ContentBox>
      </Top>
      <Story>
        <Category>원민픽 닭찌...</Category>
      </Story>
      <Separator></Separator>
      <Bottom>
        <BottomTop>
          <Icon>
            <FontAwesomeIcon icon={faTh} />
            <span>게시물</span>
          </Icon>
          <Icon>
            <FontAwesomeIcon icon={faVideo} />
            <span>IGTV</span>
          </Icon>
          <Icon>
            <FontAwesomeIcon icon={faBookmark} />
            <span>저장됨</span>
          </Icon>
          <Icon>
            <FontAwesomeIcon icon={faUserTag} />
            <span>태그됨</span>
          </Icon>
        </BottomTop>
        <Photos>
          {photos[0] ? (
            photos.map((photo: any, idx: number) => (
              <ProfilePhoto photo={photo} key={idx} />
            ))
          ) : (
            <div>There is no Photos.</div>
          )}
        </Photos>
      </Bottom>
    </Container>
  );
};

export default ProfileDetail;
