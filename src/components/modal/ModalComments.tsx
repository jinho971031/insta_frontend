import { useMutation } from "@apollo/client";
import styled from "styled-components";
import { DELETE_COMMENT_MUTATION } from "../../gql/mutations";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";

import Avatar from "../shared/Avatar";
import Caption from "../shared/Caption";

import { useState } from "react";

const Container = styled.div`
  display: flex;
  padding: 15px;
`;

const ModalButton = styled.div`
  width: 40px;
  height: 40px;
  display: none;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  background-color: ${(props) => props.theme.bgColor};
  z-index: 99;
  opacity: 0.7;
  position: absolute;
  right: 25px;
`;

const ToUser = styled.div`
  display: flex;
  cursor: pointer;
  align-items: center;
  span {
    margin-left: 10px;
  }
  margin-right: 10px;
`;

const RightBox = styled.div`
  width: 100%;
  position: relative;
  :hover {
    .menu {
      display: flex;
    }
  }
`;

const CaptionBox = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  word-break: break-all;
  position: relative;
`;

//------------------------------------------------

const ModalMenu = styled.div<ModalProps>`
  display: ${(props) => (props.isMenuOn ? "flex" : "none")};
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 99;
  background-color: rgba(0, 0, 0, 0.5);
`;

const Wrapper = styled.div`
  background-color: ${(props) => props.theme.bgColor};
  border-radius: 10px;
`;

const SDelete = styled.div`
  width: 400px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 10px 10px 0 0;
  cursor: pointer;
  padding: 15px;
  color: red;
  font-weight: 700;
  :hover {
    background-color: ${(props) => props.theme.hoverColor};
  }
`;

const SReport = styled.div`
  width: 400px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 15px;
  color: red;
  font-weight: 700;
`;

const Close = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  padding: 15px;
  :hover {
    background-color: ${(props) => props.theme.hoverColor};
  }
  border-top: 0.5px solid ${(props) => props.theme.hoverColor};
  border-radius: 0 0 10px 10px;
`;

//--------------------------------------------------

const LikeBox = styled.div`
  margin-top: 7px;
  margin-left: 15px;
`;

const PhotoCreatedAt = styled.div`
  font-size: 12px;
  opacity: 0.5;
  margin-top: 10px;
`;

//--------------------------------------------------

const ModalComments = ({ photoId, oneComment, index }: any) => {
  const updateDeleteComment = (cache: any, result: any) => {
    const {
      data: {
        deleteComment: { ok },
      },
    } = result;

    if (ok && oneComment) {
      const id = `Comment:${oneComment.id}`;
      console.log("id:", id);
      cache.evict({ id });
      cache.gc();
      cache.modify({
        id: `Photo:${photoId}`,
        fields: {
          commentCount(prev: any) {
            return prev - 1;
          },
        },
      });
      setOnMenu(false);
    }
  };

  const [deleteCommentMutation] = useMutation(DELETE_COMMENT_MUTATION, {
    variables: {
      commentId: oneComment.id,
    },
    update: updateDeleteComment,
  });

  const onDelete = () => {
    deleteCommentMutation();
  };

  const [onMenu, setOnMenu] = useState(false);

  return (
    <Container key={index}>
      <ToUser>
        <Avatar url={oneComment.user?.avatar} size="lg" />
      </ToUser>

      <RightBox>
        <ModalButton className="menu" onClick={() => setOnMenu(true)}>
          <FontAwesomeIcon icon={faEllipsisH} />
        </ModalButton>
        <CaptionBox>
          <Caption
            caption={oneComment.comment}
            username={oneComment.user.username}
          />
          <ModalMenu isMenuOn={onMenu}>
            {onMenu ? (
              <Wrapper>
                {oneComment.isMine ? (
                  <SDelete onClick={onDelete}>삭제</SDelete>
                ) : (
                  <SReport>신고</SReport>
                )}
                <Close onClick={() => setOnMenu(false)}>취소</Close>
              </Wrapper>
            ) : null}
          </ModalMenu>
          <LikeBox>
            <FontAwesomeIcon icon={faHeart} size="sm" />
          </LikeBox>
        </CaptionBox>

        <PhotoCreatedAt>{oneComment.createdAt}</PhotoCreatedAt>
      </RightBox>
    </Container>
  );
};

export default ModalComments;
