import styled, { keyframes } from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import {
  faBookmark,
  faComment,
  faPaperPlane,
} from "@fortawesome/free-regular-svg-icons";

import PhotoLike from "../feed/PhotoLike";
import Avatar from "../shared/Avatar";
import { FatText } from "../shared/shared";
import Caption from "../shared/Caption";
import CreateComment from "../feed/CreateComment";
import TogglePhotoLike from "../shared/togglePhotoLike";
import ModalComments from "./ModalComments";

const modalShow = keyframes`
  from {
    opacity: 0;
    margin-top: -50px;
  }
  to {
    opacity: 1;
    margin-top: 0;
  }
`;
const modalBgShow = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`;

const Container = styled.div<ModalProps>`
  display: ${(props) => (props.isModalOn ? "flex" : "none")};
  align-items: center;
  animation: ${modalBgShow} 0.3s;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 98;
  background-color: rgba(0, 0, 0, 0.5);
`;

// openModal modal : modal

const CloseButton = styled.button`
  position: absolute;
  top: 20px;
  right: 20px;
  width: 30px;
  height: 30px;
  color: white;
  background: none;
  border: none;
  cursor: pointer;
  :focus {
    border: none;
    outline: none;
  }
`;

const Section = styled.section`
  display: flex;
  margin: 0 auto;
  background-color: ${(props) => props.theme.bgColor};
  animation: ${modalShow} 0.3s;
  overflow: hidden;
`;

const Images = styled.div`
  width: 500px;
  display: flex;
  justify-content: center;
  align-items: center;
  user-select: none;
`;

const Img = styled.img`
  width: 100%;
`;

const Contents = styled.div`
  width: 335px;
  border-left: 0.5px solid ${(props) => props.theme.litegray};
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const User = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 0.5px solid ${(props) => props.theme.litegray};
  padding: 15px;
`;

const UserLink = styled.div`
  display: flex;
  cursor: pointer;
  align-items: center;
  span {
    margin-left: 10px;
  }
  margin-right: 10px;
`;

//------------------[ 박스 스타일링 ]------------------------//

const TopBox = styled.div``;

const BottomBox = styled.div``;

const RightBox = styled.div`
  width: 100%;
`;

const FlexLeftBox = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  span {
    margin-left: 10px;
  }
`;

const FlexRightBox = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  span {
    margin-left: 10px;
  }
`;

//------------------[  ]------------------------//

const CaptionBar = styled.div`
  display: flex;
  padding: 15px;
`;

const CaptionBox = styled.div`
  display: flex;
  width: 90%;
  word-break: break-all;
  margin-right: 10px;
  position: relative;
  :hover {
    div {
      display: flex;
    }
  }
`;

const PhotoCreatedAt = styled.div`
  font-size: 12px;
  opacity: 0.5;
  margin-top: 10px;
`;

const CommentBar = styled.div`
  width: 100%;
  max-height: 300px;

  overflow-y: scroll;
  height: calc(100% - 32px);
  ::-webkit-scrollbar {
    display: none;
  }
  --ms-overflow-style: none;
  scrollbar-width: none;
`;

const IconBar = styled.div`
  display: flex;
  border-top: 0.5px solid ${(props) => props.theme.litegray};
  padding: 10px 15px;
`;

const PhotoAction = styled.div`
  :not(:last-child) {
    margin-right: 15px;
  }
  cursor: pointer;
`;

const LikeBar = styled.div`
  border-bottom: 0.5px solid ${(props) => props.theme.litegray};
  padding: 0 15px;
`;

const ModalPhoto = ({
  isModalOn,
  closeModal,
  id,
  file,
  user,
  caption,
  comments,
  isLiked,
  likeCount,
  createdAt,
}: any) => {
  const isOpen = Boolean(isModalOn);

  return (
    <Container isModalOn={isOpen}>
      {isOpen ? (
        <>
          <CloseButton onClick={closeModal}>
            <FontAwesomeIcon icon={faTimes} size="lg" />
          </CloseButton>
          <Section>
            <Images>
              <Img src={file} alt="file" />
            </Images>
            <Contents>
              <TopBox>
                <User>
                  <UserLink>
                    <Avatar url={user.avatar} size="lg" />
                    <FatText>{user.username}</FatText>
                  </UserLink>
                  <FlexRightBox>
                    <FontAwesomeIcon icon={faEllipsisH} />
                  </FlexRightBox>
                </User>

                <CaptionBar>
                  <UserLink>
                    <Avatar url={user.avatar} size="lg" />
                  </UserLink>
                  <RightBox>
                    <CaptionBox>
                      <Caption
                        caption={caption}
                        username={user.username}
                        key={id}
                      />
                    </CaptionBox>
                    <PhotoCreatedAt>{createdAt}</PhotoCreatedAt>
                  </RightBox>
                </CaptionBar>

                <CommentBar>
                  {comments.map((oneComment: any, index: any) => (
                    <ModalComments
                      oneComment={oneComment}
                      key={index}
                      photoId={id}
                    />
                  ))}
                </CommentBar>
              </TopBox>

              <BottomBox>
                <IconBar>
                  <FlexLeftBox>
                    <TogglePhotoLike
                      id={id}
                      isLiked={isLiked}
                      likeCount={likeCount}
                    />
                    <PhotoAction>
                      <FontAwesomeIcon icon={faComment} size="2x" />
                    </PhotoAction>
                    <PhotoAction>
                      <FontAwesomeIcon icon={faPaperPlane} size="2x" />
                    </PhotoAction>
                  </FlexLeftBox>
                  <FlexRightBox>
                    <PhotoAction>
                      <FontAwesomeIcon icon={faBookmark} size="2x" />
                    </PhotoAction>
                  </FlexRightBox>
                </IconBar>
                <LikeBar>
                  {likeCount > 0 ? (
                    <PhotoLike likeCount={likeCount} id={id} />
                  ) : null}
                </LikeBar>
                <CreateComment
                  type="text"
                  placeholder="댓글 달기..."
                  photoId={id}
                  comments={comments}
                />
              </BottomBox>
            </Contents>
          </Section>
        </>
      ) : null}
    </Container>
  );
};

export default ModalPhoto;
