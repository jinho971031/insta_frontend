import { faBookmark, faUser } from "@fortawesome/free-regular-svg-icons";
import { faCog, faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { logUserOut } from "../../apollo";
import useUser from "../../hooks/useUser";

const Container = styled.div`
  width: 160px;
  background-color: ${(props) => props.theme.boxColor};
  position: absolute;
  top: 30px;
  left: -100px;
  border: 0.5px solid ${(props) => props.theme.borderColor};
  border-radius: 7px;
  box-shadow: 0 0 5px 1px rgba(var(--jb7, 0, 0, 0), 0.0975);
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Profile = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  padding: 10px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  :hover {
    background-color: ${(props) => props.theme.hoverColor};
  }
`;

const Menu = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  padding: 10px;
  :last-child {
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    border-top: 0.5px solid ${(props) => props.theme.borderColor};
  }
  :hover {
    background-color: ${(props) => props.theme.hoverColor};
  }
  span {
    margin-left: 7px;
  }
`;

const Icon = styled.div`
  margin: 0 10px;
`;

const SLink = styled(Link)`
  width: 100%;
  color: ${(props) => props.theme.fontColor};
`;

const ModalProfile = () => {
  const { data } = useUser();
  return (
    <Container>
      <Wrapper>
        <SLink to={`/users/${data?.me?.username}`}>
          <Profile>
            <Icon>
              <FontAwesomeIcon icon={faUser} />
            </Icon>
            프로필
          </Profile>
        </SLink>
        <Menu>
          <Icon>
            <FontAwesomeIcon icon={faBookmark} />
          </Icon>
          저장됨
        </Menu>
        <Menu>
          <Icon>
            <FontAwesomeIcon icon={faCog} />
          </Icon>
          설정
        </Menu>
        <Menu>
          <Icon>
            <FontAwesomeIcon icon={faSyncAlt} />
          </Icon>
          계정 전환
        </Menu>
        <Menu onClick={() => logUserOut()}>
          <span>로그아웃</span>
        </Menu>
      </Wrapper>
    </Container>
  );
};

export default ModalProfile;
