import styled from "styled-components";
import { Link } from "react-router-dom";
import { BaseBox } from "../shared/shared";

type BottomProps = {
  weight: string;
};

const SBottomBox = styled(BaseBox)<BottomProps>`
  padding: 23px 40px 23px 40px;
  text-align: center;
  a {
    margin-left: 5px;
    color: ${(props) => props.theme.blue};
    font-weight: ${(props) => props.weight && props.weight};
  }
`;

function BottomBox({ cta, link, linkText, fontWeight }: BottomBoxProps) {
  if (!fontWeight) {
    fontWeight = "600";
  }
  return (
    <SBottomBox weight={fontWeight}>
      <span>{cta}</span>
      <Link to={link}>{linkText}</Link>
    </SBottomBox>
  );
}

export default BottomBox;
