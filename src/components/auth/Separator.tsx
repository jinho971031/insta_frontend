import styled from "styled-components";

const SSeparator = styled.div`
  margin: 20px;
  text-transform: uppercase;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  div {
    width: 100%;
    height: 1px;
    background-color: ${(props) => props.theme.seprator};
  }
  span {
    margin: 0 18px;
    flex-shrink: 0;
    font-size: 12px;
    font-weight: 600;
    color: ${(props) => props.theme.gray};
  }
`;

function Separator() {
  return (
    <SSeparator>
      <div></div>
      <span>또는</span>
      <div></div>
    </SSeparator>
  );
}

export default Separator;
