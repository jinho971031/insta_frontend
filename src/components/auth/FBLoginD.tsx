import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

const SFacebookLogin = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.blue};
  color: ${(props) => props.theme.bgColor};
  border-radius: 4px;
  padding: 10px;
  user-select: none;

  span {
    margin-left: 10px;
    font-weight: 600;
  }
`;

function FacebookLogin() {
  return (
    <SFacebookLogin>
      <FontAwesomeIcon icon={faFacebookSquare} />
      <span>Facebook으로 로그인</span>
    </SFacebookLogin>
  );
}

export default FacebookLogin;
