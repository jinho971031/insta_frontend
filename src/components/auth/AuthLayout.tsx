import styled from "styled-components";
import DarkModeButton from "../shared/DarkModeButton";

const Container = styled.div`
  display: flex;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Wrapper = styled.div`
  max-width: 350px;
  width: 100%;
`;

const Footer = styled.footer`
  margin-top: 30px;
`;

function AuthLayout({ children }: Props) {
  return (
    <Container>
      <Wrapper> {children} </Wrapper>
      <Footer>
        <DarkModeButton />
      </Footer>
    </Container>
  );
}

export default AuthLayout;
