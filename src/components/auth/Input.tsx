import styled from "styled-components";

const Input = styled.input<InputProps>`
  width: 100%;
  color: ${(props) => props.theme.fontColor};
  background-color: ${(props) => props.theme.bgColor};
  border: 0.5px solid
    ${(props) => (props.hasError ? "tomato" : props.theme.borderColor)};
  border-radius: 3px;
  padding: 10px;
  margin-top: 5px;
  box-sizing: border-box;
  &:placeholder {
    font-size: 12px;
  }
  &:focus {
    border-color: rgb(38, 38, 38);
  }
`;

export default Input;
