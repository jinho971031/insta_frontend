import {
  faTimesCircle,
  faCheckCircle,
} from "@fortawesome/free-regular-svg-icons";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";

const IconBox = styled.div`
  background-color: ${(props) => props.theme.bgColor};
  margin-right: 5px;
`;

export function InValid() {
  return (
    <IconBox>
      <FontAwesomeIcon icon={faTimesCircle} size="2x" color="red" />
    </IconBox>
  );
}

export function Valid() {
  return (
    <IconBox>
      <FontAwesomeIcon icon={faCheckCircle} size="2x" color="gray" />
    </IconBox>
  );
}

export function Retry() {
  return (
    <IconBox>
      <FontAwesomeIcon icon={faSyncAlt} size="2x" color="#0095f6" />
    </IconBox>
  );
}
