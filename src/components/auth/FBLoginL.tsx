import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

const SFacebookLogin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${(props) => props.theme.darkBlue};
  margin-bottom: 20px;
  user-select: none;

  span {
    margin-left: 10px;
    font-weight: 600;
  }
`;

function FacebookLogin() {
  return (
    <SFacebookLogin>
      <FontAwesomeIcon icon={faFacebookSquare} size="2x" />
      <span>Facebook으로 로그인</span>
    </SFacebookLogin>
  );
}

export default FacebookLogin;
