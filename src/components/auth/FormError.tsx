import styled from "styled-components";

const SFormError = styled.span`
  color: tomato;
  font-weight: 600;
  font-size: 12px;
  margin: 15px 0 10px 0;
`;

function FormError({ message }: FormError) {
  return message === "" || !message ? null : <SFormError>{message}</SFormError>;
}

export default FormError;
