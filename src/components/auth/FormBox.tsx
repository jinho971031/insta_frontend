import styled from "styled-components";
import { BaseBox } from "../shared/shared";

const SFormBox = styled(BaseBox)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-bottom: 10px;
  padding: 30px 40px 23px 40px;
  form {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
  a {
    font-size: 12px;
    color: ${(props) => props.theme.darkBlue};
  }
`;

function FormBox({ children }: Props) {
  return <SFormBox>{children}</SFormBox>;
}

export default FormBox;
