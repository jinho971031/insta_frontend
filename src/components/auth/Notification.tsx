import styled from "styled-components";

const SNotification = styled.div`
  font-weight: 600;
  color: #2ecc71;
  margin-bottom: 10px;
`;

function Notification({ message }: any) {
  return message === "" || !message ? null : (
    <SNotification>{message}</SNotification>
  );
}

export default Notification;
