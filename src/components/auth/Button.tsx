import styled from "styled-components";

const Button = styled.input`
  width: 100%;
  border-radius: 5px;
  margin-top: 15px;
  background-color: ${(props) => props.theme.blue};
  color: ${(props) => props.theme.white};
  text-align: center;
  padding: 7px;
  font-weight: 600;
  box-sizing: border-box;
  user-select: none;
  opacity: ${(props) => (props.disabled ? 0.5 : 1)};
  &:hover {
    cursor: pointer;
  }
`;

export default Button;
