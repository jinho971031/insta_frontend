const routes = {
  home: "/",
  signUp: "/sign-up",
  hashtags: "/hashtags",
  users: "/users",
};

export default routes;
