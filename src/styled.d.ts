import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    fontColor: string;
    bgColor: string;
    boxColor: string;
    borderColor: string;
    seprator: string;
    link: string;
    hoverColor: string;

    black: string;
    white: string;
    litegray: string;
    blue: string;
    darkBlue: string;
    gray: string;
  }
}
