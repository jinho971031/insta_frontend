import AuthLayout from "../components/auth/AuthLayout";
import FormBox from "../components/auth/FormBox";
import FBLoginD from "../components/auth/FBLoginD";
import Separator from "../components/auth/Separator";
import Input from "../components/auth/Input";
import Button from "../components/auth/Button";
import BottomBox from "../components/auth/BottomBox";
import routes from "../routes";
import styled from "styled-components";
import { FatLink } from "../components/shared/shared";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { useForm } from "react-hook-form";
import PageTitle from "../components/shared/PageTitle";
import FormError from "../components/auth/FormError";
import { useMutation } from "@apollo/client";
import { SIGNUP_MUTATION } from "../gql/mutations";
import { useHistory } from "react-router";

const HeaderContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Subtitle = styled(FatLink)`
  font-size: 16px;
  text-align: center;
  line-height: 130%;
  margin: 10px 0;
`;

function SignUp() {
  const history = useHistory();
  const {
    register,
    handleSubmit,
    formState,
    getValues,
    setError,
    clearErrors,
  } = useForm({
    mode: "onChange",
  });
  const { errors } = formState;
  const onCompleted = (data: SignUpResult) => {
    const { username, password } = getValues();
    const {
      createAccount: { ok, error },
    } = data;

    if (ok) {
      history.push(routes.home, {
        message: "계정이 생성되었습니다. 로그인 해주세요.",
        username,
        password,
      });
    }
    setError("result", {
      message: error,
    });
  };
  const [signup, { loading }] = useMutation(SIGNUP_MUTATION, {
    onCompleted,
  });
  const onSubmit = () => {
    if (loading) {
      return;
    }
    const { email, name, username, password } = getValues();
    signup({
      variables: {
        name,
        email,
        username,
        password,
      },
    });
  };

  return (
    <AuthLayout>
      <PageTitle title="SignUp" />

      <FormBox>
        <HeaderContainer>
          <FontAwesomeIcon icon={faInstagram} size="3x" />
          <Subtitle>친구들의 사진과 동영상을 보려면 가입하세요.</Subtitle>
          <FBLoginD />
        </HeaderContainer>
        <Separator />
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            {...register("email", {
              required: "이메일을 입력해주세요.",
              pattern: {
                value: /^[0-9a-zA-Z._%+-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,6}$/i,
                message: "이메일을 형식에 맞게 작성해주세요.",
              },
              validate: (): any => {
                if (errors.result) {
                  clearErrors("result");
                }
              },
            })}
            type="text"
            placeholder="휴대폰 번호 또는 이메일 주소"
            hasError={Boolean(errors?.email?.message)}
          />
          <FormError message={errors?.email?.message} />

          <Input
            {...register("name", {
              required: "이름을 입력해주세요.",
              pattern: {
                value: /^[A-Za-z가-힣]+$/i,
                message: "문자만 입력해주세요.",
              },
              validate: (): any => {
                if (errors.result) {
                  clearErrors("result");
                }
              },
            })}
            type="text"
            placeholder="성명"
            hasError={Boolean(errors?.name?.message)}
          />
          <FormError message={errors?.name?.message} />
          <Input
            {...register("username", {
              required: "사용자 이름을 입력해주세요.",
              minLength: 5,
              pattern: {
                value: /^[A-Za-z0-9]+$/i,
                message: "영문, 숫자만 입력해주세요.",
              },
              validate: (): any => {
                if (errors.result) {
                  clearErrors("result");
                }
              },
            })}
            type="text"
            placeholder="사용자 이름"
            hasError={Boolean(errors?.username?.message)}
          />
          <FormError message={errors?.username?.message} />
          <Input
            {...register("password", {
              required: "비밀번호를 입력해주세요.",
              validate: (): any => {
                if (errors.result) {
                  clearErrors("result");
                }
              },
              // pattern: {
              //   value:
              //     /^.*(?=^.{8,15}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/,
              //   message:
              //     "특수문자, 문자, 숫자로 이루어진 8~15자리만 사용가능합니다.",
              // },
            })}
            type="password"
            placeholder="비밀번호"
            hasError={Boolean(errors?.password?.message)}
          />
          <FormError message={errors?.password?.message} />
          <Button
            type="submit"
            value={loading ? "로딩중" : "가입"}
            disabled={!formState.isValid || loading}
          />
          <FormError message={errors.result?.message} />
        </form>
      </FormBox>

      <BottomBox
        cta="계정이 있으신가요?"
        link={routes.home}
        linkText="로그인"
        fontWeight="400"
      />
    </AuthLayout>
  );
}

export default SignUp;
