import { useQuery } from "@apollo/client";
import { useParams } from "react-router-dom";
import ProfileDetail from "../components/profile/ProfileDetail";

import { SEE_PROFILE_QUERY } from "../gql/queries";

const Profile = () => {
  const { username }: any = useParams();
  const { data } = useQuery(SEE_PROFILE_QUERY, {
    variables: { username },
  });

  return data ? <ProfileDetail data={data.seeProfile} /> : <div>No User</div>;
};

export default Profile;
