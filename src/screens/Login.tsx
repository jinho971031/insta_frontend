import routes from "../routes";
import AuthLayout from "../components/auth/AuthLayout";
import Button from "../components/auth/Button";
import Separator from "../components/auth/Separator";
import Input from "../components/auth/Input";
import FBLoginL from "../components/auth/FBLoginL";
import FormBox from "../components/auth/FormBox";
import BottomBox from "../components/auth/BottomBox";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { useForm } from "react-hook-form";
import PageTitle from "../components/shared/PageTitle";
import FormError from "../components/auth/FormError";
import { useMutation } from "@apollo/client";
import { LOGIN_MUTATION } from "../gql/mutations";
import { logUserIn } from "../apollo";
import { useLocation } from "react-router";
import Notification from "../components/auth/Notification";

const LogoBox = styled.div`
  margin-bottom: 30px;
`;

function Login() {
  const location: any = useLocation();
  const {
    register,
    handleSubmit,
    formState,
    getValues,
    setError,
    clearErrors,
  } = useForm({
    mode: "onChange",
    defaultValues: {
      username: location?.state?.username || "",
      password: location?.state?.password || "",
      result: null,
    },
  });

  // const history = useHistory();
  const onCompleted = (data: LoginResult) => {
    const {
      login: { ok, error, token },
    } = data;

    if (ok) {
      if (token) {
        logUserIn(token);
      }
    }
    return setError("result", {
      type: "manual",
      message: error,
    });
  };
  const [login, { loading }] = useMutation(LOGIN_MUTATION, {
    onCompleted,
  });

  const onSubmit = () => {
    if (loading) {
      return;
    }
    const { username, password } = getValues();
    login({
      variables: {
        username,
        password,
      },
    });
  };

  const { errors } = formState;

  return (
    <AuthLayout>
      <PageTitle title="Login" />
      <FormBox>
        <LogoBox>
          <FontAwesomeIcon icon={faInstagram} size="3x" />
        </LogoBox>
        <Notification message={location?.state?.message} />
        <form onSubmit={handleSubmit(onSubmit)}>
          <Input
            {...register("username", {
              required: "사용자 이름을 입력해주세요.",
              validate: (): any => {
                if (errors.result) {
                  clearErrors("result");
                }
              },
            })}
            type="text"
            placeholder="사용자 이름 또는 이메일"
            hasError={Boolean(errors?.username?.message)}
          />
          <FormError message={errors.username?.message} />
          <Input
            {...register("password", {
              required: "비밀번호를 입력해주세요.",
              validate: (): any => {
                if (errors.result) {
                  clearErrors("result");
                }
              },
            })}
            type="password"
            placeholder="비밀번호"
            hasError={Boolean(errors?.password?.message)}
          />
          <FormError message={errors.password?.message} />
          <Button
            type="submit"
            value={loading ? "로딩중..." : "로그인"}
            disabled={!formState.isValid || loading}
          />
          <FormError message={errors.result?.message} />
        </form>
        <Separator />
        <FBLoginL />
        <a href="b">비밀번호를 잊으셨나요?</a>
      </FormBox>
      <BottomBox
        cta="계정이 없으신가요?"
        link={routes.signUp}
        linkText="가입하기"
      />
    </AuthLayout>
  );
}

export default Login;
