import { useQuery } from "@apollo/client";
import styled from "styled-components";
import { FEED_QUERY } from "../gql/queries";
import { Feed_Feed } from "../__generated__/Feed";
import Photo from "../components/feed/Photo";
import PageTitle from "../components/shared/PageTitle";

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  padding-top: 50px;
`;

const Wrapper = styled.div`
  max-width: 600px;
  width: 100%;
`;

const Story = styled.div`
  background-color: ${(props) => props.theme.boxColor};
  border: 0.5px solid ${(props) => props.theme.borderColor};
  padding: 10px 0 10px 20px;
  margin-bottom: 25px;
`;
const StoryUsers = styled.div`
  display: flex;
`;
const StoryUser = styled.div`
  margin-right: 10px;
`;

// ----------------------------------------

const SFeed = styled.div``;

//-----------------------------------------

const Feed = () => {
  const { data } = useQuery(FEED_QUERY);

  return (
    <Container>
      <PageTitle title="Feed" />
      <Wrapper>
        <Story>
          <StoryUsers>
            <StoryUser>Avatar Username</StoryUser>
            <StoryUser>Avatar Username</StoryUser>
          </StoryUsers>
        </Story>
        <SFeed>
          {data?.Feed.map((photo: Feed_Feed) => (
            <Photo key={photo.id} {...photo} />
          ))}
        </SFeed>
      </Wrapper>
    </Container>
  );
};

export default Feed;
