import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Feed from "./screens/Feed";
import Login from "./screens/Login";
import NotFound from "./screens/NotFound";
import { client, darkModeVar, hasTokenVar, modalVar } from "./apollo";
import { ApolloProvider, useReactiveVar } from "@apollo/client";
import { ThemeProvider } from "styled-components";
import { darkTheme, GlobalStyles, lightTheme } from "./styles";
import SignUp from "./screens/SignUp";
import routes from "./routes";
import { HelmetProvider } from "react-helmet-async";
import Layout from "./components/shared/Layout";
import Hashtags from "./screens/Hashtags";
import Profile from "./screens/Profile";

function App() {
  const isLoggedIn = useReactiveVar(hasTokenVar);
  const darkMode = useReactiveVar(darkModeVar);
  const isModalOn = Boolean(useReactiveVar(modalVar));

  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
        <GlobalStyles isModalOn={isModalOn} />
        <HelmetProvider>
          <Router>
            <Switch>
              <Route path={routes.home} exact>
                {isLoggedIn ? (
                  <Layout>
                    <Feed />
                  </Layout>
                ) : (
                  <Login />
                )}
              </Route>
              {!isLoggedIn ? (
                <Route path={routes.signUp} exact>
                  <SignUp />
                </Route>
              ) : null}
              <Route path={routes.hashtags}>
                <Hashtags />
              </Route>
              <Route path={`/users/:username`}>
                <Layout>
                  <Profile />
                </Layout>
              </Route>
              <Route>
                <NotFound />
              </Route>
            </Switch>
          </Router>
        </HelmetProvider>
      </ThemeProvider>
    </ApolloProvider>
  );
}

export default App;
