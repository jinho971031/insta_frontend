import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  makeVar,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

const TOKEN = "authorization";

export const hasTokenVar = makeVar(localStorage.getItem(TOKEN));

export const logUserIn = (token: string) => {
  localStorage.setItem(TOKEN, token);
  hasTokenVar("true");
};

export const logUserOut = () => {
  localStorage.removeItem(TOKEN);
  window.location.reload();
  hasTokenVar(null);
};

//--------------------------------------------------

const ISDARK = "isdark";

export const darkModeVar = makeVar(localStorage.getItem(ISDARK));

export const DarkMode = () => {
  localStorage.setItem(ISDARK, "enabled");
  darkModeVar("true");
};

export const LiteMode = () => {
  localStorage.removeItem(ISDARK);
  darkModeVar(null);
};

//--------------------------------------------------

const MODAL = "isModalOn";

export const modalVar = makeVar(sessionStorage.getItem(MODAL));

export const onModal = () => {
  sessionStorage.setItem(MODAL, "enabled");
  modalVar("true");
};

export const offModal = () => {
  sessionStorage.removeItem(MODAL);
  modalVar(null);
};

//---------------------------------------------------

//---------------------------------------------------

const httpLink = createHttpLink({
  uri: "http://localhost:4000/graphql",
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(TOKEN);
  if (!token) {
    return {
      headers: { ...headers },
    };
  }
  return {
    headers: {
      ...headers,
      authorization: token,
    },
  };
});

// concat 으로 authLink, httpLink 연결해주기

// graphql, apollo & react 연결하기 위한 ApolloClient 생성
export const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache({
    typePolicies: {
      User: {
        keyFields: (obj) => `User:${obj.username}`,
      },
    },
  }),
});
