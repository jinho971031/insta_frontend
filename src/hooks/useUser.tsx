import { useQuery, useReactiveVar } from "@apollo/client";
import { useEffect } from "react";
import { hasTokenVar, logUserOut } from "../apollo";
import { ME_QUERY } from "../gql/queries";

// 로컬스토리지를 통해 로그인 한 경우에만 실행되는 쿼리

function useUser() {
  const hasToken = useReactiveVar(hasTokenVar);

  //hasToken이 없을때, 쿼리를 실행하지 않음
  const { data } = useQuery(ME_QUERY, {
    skip: !hasToken,
  });

  useEffect(() => {
    if (data?.me === null) {
      logUserOut();
    }
  }, [data]);
  return { data };
}

export default useUser;
