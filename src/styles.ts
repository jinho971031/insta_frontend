import { DefaultTheme, createGlobalStyle } from "styled-components";
import reset from "styled-reset";

export const lightTheme: DefaultTheme = {
  fontColor: "rgb(38,38,38)",
  bgColor: "#fafafa",
  boxColor: "white",
  borderColor: "rgb(219, 219, 219)",
  seprator: "rgb(219,219,219)",
  link: "rgb(142,142,142)",
  hoverColor: "rgb(219,219,219)",

  black: "rgb(38,38,38)",
  white: "#fafafa",
  litegray: "rgb(219, 219, 219)",
  blue: "#0095f6",
  darkBlue: "#385185",
  gray: "#8e8e8e",
};

export const darkTheme: DefaultTheme = {
  fontColor: "#fafafa",
  bgColor: "#131722",
  boxColor: "#1E222D",
  borderColor: "#1E222D",
  seprator: "#8e8e8e",
  link: "#fafafa",
  hoverColor: "#262B3E",

  black: "rgb(38,38,38)",
  white: "#fafafa",
  litegray: "rgb(219, 219, 219)",
  blue: "#0095f6",
  darkBlue: "#385185",
  gray: "#8e8e8e",
};

export const GlobalStyles = createGlobalStyle<ModalProps>`
    ${reset}
    input {
      all: unset;
    }
    * {
      box-sizing: border-box; 
    }
    body {
      background-color: ${(props) => props.theme.bgColor};
      color: ${(props) => props.theme.fontColor};
      font-size: 14px;
      font-family: "Open Sans", sans-serif;
      overflow: ${(props) => (props.isModalOn ? "hidden" : "scroll")};
    }
    a {
      text-decoration: none;
      color: inherit;
    }
`;
