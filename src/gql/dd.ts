import { gql } from "@apollo/client";

const EHTER = gql`
  query {
    id
    file
    likeCount
    commentCount
    caption
    isLiked
    createdAt

    user {
      avatar
      username
    }
    
    comments {
      id
      user {
        username
        avatar
      }
      comment
      isMine
      createdAt
    }

    ///////////////////////////////////
    likes {
      id
      user {
        username
        avatar
      }
    }
    
    isMine
  }
`;
