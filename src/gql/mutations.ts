import gql from "graphql-tag";

export const LOGIN_MUTATION = gql`
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      ok
      token
      error
    }
  }
`;

export const SIGNUP_MUTATION = gql`
  mutation signup(
    $name: String!
    $email: String!
    $username: String!
    $password: String!
  ) {
    createAccount(
      name: $name
      email: $email
      username: $username
      password: $password
    ) {
      ok
      error
    }
  }
`;

export const TOGGLE_PHOTO_LIKE_MUTATION = gql`
  mutation togglePhotoLike($photoId: Int!) {
    togglePhotoLike(photoId: $photoId) {
      ok
      error
    }
  }
`;

export const CREATE_COMMENT_MUTATION = gql`
  mutation createComment($photoId: Int!, $comment: String!) {
    createComment(photoId: $photoId, comment: $comment) {
      ok
      error
      id
    }
  }
`;

export const DELETE_COMMENT_MUTATION = gql`
  mutation deleteComment($commentId: Int!) {
    deleteComment(commentId: $commentId) {
      ok
      error
    }
  }
`;

export const FOLLOW_MUTATION = gql`
  mutation follow($username: String!) {
    follow(username: $username) {
      ok
      error
    }
  }
`;

export const UNFOLLOW_MUTATION = gql`
  mutation unfollow($username: String!) {
    unfollow(username: $username) {
      ok
      error
    }
  }
`;
