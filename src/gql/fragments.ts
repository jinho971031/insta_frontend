import { gql } from "@apollo/client";

export const COMMENT_FRAGMENT = gql`
  fragment CommentFragment on Comment {
    id
    user {
      username
      avatar
    }
    comment
    isMine
    createdAt
  }
`;

export const PHOTO_FRAGMENT = gql`
  fragment PhotoFragment on Photo {
    id
    file
    likeCount
    commentCount
    isLiked
    caption
    createdAt
    user {
      avatar
      username
    }

    comments {
      ...CommentFragment
    }
  }
  ${COMMENT_FRAGMENT}
`;
