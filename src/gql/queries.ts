import gql from "graphql-tag";
import { PHOTO_FRAGMENT } from "./fragments";

export const FEED_QUERY = gql`
  query Feed($lastID: Int) {
    Feed(lastID: $lastID) {
      ...PhotoFragment
      likes {
        id
        user {
          username
          avatar
        }
      }
    }
  }
  ${PHOTO_FRAGMENT}
`;

export const ME_QUERY = gql`
  query me {
    me {
      username
      avatar
    }
  }
`;

export const SEE_PHOTO_LIKES_QUERY = gql`
  query seePhotoLikes($photoId: Int!, $lastID: Int) {
    seePhotoLikes(photoId: $photoId, lastID: $lastID) {
      username
      avatar
    }
  }
`;

export const SEE_PROFILE_QUERY = gql`
  query seeProfile($username: String!) {
    seeProfile(username: $username) {
      avatar
      username
      totalPhotos
      totalFollowers
      totalFollowing
      isMe
      isFollowing
      name
      photos {
        ...PhotoFragment
      }
    }
  }
  ${PHOTO_FRAGMENT}
`;
