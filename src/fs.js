import fs from "fs";

// 파일 읽기

fs.readFile("filename", [options], callback);

// "filename"파일을, [options]을 적용해 읽은후, callback 함수를 실행!

fs.readFileSync("finename", [options]);

// "filename"파일을, [options]을 적용해 읽은후, 문자열 반환

// Sync : 동기적 읽기를 뜻함 ( 한 작업을 마치기 전까지, 다른 작업 불가 )

// 파일 쓰기

fs.writeFile("filename", data, [options], callback);

// "filename"파일에, [options]을 적용해 data내용을 작성하고, callback함수 실행!

fs.writeFileSync("filename", data, [options]);

// "filename"파일에, [options]을 적용해 data내용을 작성

// 예외처리

/* 
  파일 입출력은, 다양한 윈인으로 예외가 존재해 에러가 발생할 확률이 높다.
  ( 권한이 없거나, 파일이 없거나, 디스크 용량이 부족하거나 )
  그러므로, try/catch 문을 활용해, 예외 처리를 해주거나, (동기식)
  콜백함수에 err를 감지해 조건문으로 예외 처리를 해주어야 한다. (비동기적)
*/

// 동기식 예외처리
try {
  const file = fs.readFile("filename", [options]);
  console.log(file);
} catch (err) {
  console.log(err);
}

// 비동기식 예외처리
const file = fs.readFile("filename", [options], function (err, data) {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
