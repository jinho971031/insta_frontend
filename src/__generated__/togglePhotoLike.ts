/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: togglePhotoLike
// ====================================================

export interface togglePhotoLike_togglePhotoLike {
  __typename: "MutationResult";
  ok: boolean;
  error: string | null;
}

export interface togglePhotoLike {
  togglePhotoLike: togglePhotoLike_togglePhotoLike | null;
}

export interface togglePhotoLikeVariables {
  photoId: number;
}
