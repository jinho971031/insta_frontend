/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Feed
// ====================================================

export interface Feed_Feed_user {
  __typename: "User";
  username: string;
  avatar: string | null;
}

export interface Feed_Feed_likes_user {
  __typename: "User";
  username: string;
  avatar: string | null;
}

export interface Feed_Feed_likes {
  __typename: "PhotoLike";
  id: number;
  user: Feed_Feed_likes_user;
}

export interface Feed_Feed_comments_user {
  __typename: "User";
  username: string;
  avatar: string | null;
}

export interface Feed_Feed_comments {
  __typename: "Comment";
  id: number;
  user: Feed_Feed_comments_user;
  comment: string;
}
export interface Feed_Feed {
  __typename: "Photo";
  id: number;
  user: Feed_Feed_user;
  file: string;
  caption: string | null;
  likeCount: number;
  likes: (Feed_Feed_likes | null)[] | null;
  comments: (Feed_Feed_comments | null)[] | null;
  commentCount: number;
  createdAt: string;
  isMine: boolean;
  isLiked: boolean;
}

export interface Feed {
  Feed: (Feed_Feed | null)[] | null;
}

export interface FeedVariables {
  lastID?: number | null;
}
