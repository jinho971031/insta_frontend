/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: signup
// ====================================================

export interface signup_createAccount {
  __typename: "CreateAccountResult";
  ok: boolean;
  error: string | null;
}

export interface signup {
  createAccount: signup_createAccount | null;
}

export interface signupVariables {
  name: string;
  email: string;
  username: string;
  password: string;
}
