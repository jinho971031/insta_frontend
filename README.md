# Instaclone Web

- [x] Router
- [x] Authentication
- [x] Arch.
- [x] Styles
- [ ] Log in

# Route

## [/]

- [login] Feed
- [logout] login / signup

## [/username]

- [login / logout] Profile

# `npm start`

# styled-components 상속 (extend) 하기

# styled-components 안의 html 태그 타겟팅 하기

# styled-components 사용시 주의 사항

- [1] 컴포넌트는 무조건 대문자로 시작해야만 한다!

### createComment 기능

- [1] 댓글 추가란 외형 만들기
  (1) 텍스트 입력 박스 만들기
  (2) 제출 버튼 만들기
  (3) 제출 버튼 활성화 기능 만들기

- [2] 댓글 DB에 추가하기
  (1) useMutation 사용하기

- [3] 추가한 댓글 캐시로 만들어서 화면에 보여주기 ( 화면 보여주기 용도 )
  ( Feed의 comment 형식으로 맞춰 주어야함 )
  (1) 해당 댓글 id 값 DB에서 가져오기
  (2) user DB에서 쿼리로 만들기
  (3) 입력한 텍스트 가져오기 ( DB 경유 X )

### Modal 기능

- [1] 댓글 더보기 클릭시 모달 창 띄움 ( Comments - link, onClick )
  모달 상태를 관리하는 State를 만든다. ( 스테이트를 만든 컴포넌트에서 관리가능 )
  OR
- [2] 모달 창 띄웠을 시 작업
  (1) 피드 스크롤 안되게하기 ( Feed - overflow : hidden )
  (2) 댓글 스크롤 기능 넣기 ( Modal - overflow : scroll )
  (3) 댓글 스크롤바 숨기기 ( Modal - webkit-scrollbar, display : none )

### 사용한 쿼리

- [1] Feed : 사용자와, 사용자가 팔로우한 사람들 사진 전부 확인
- [2] Me : 현재 사용자 정보 확인
- [3] seePhotoLikes : 사진에 좋아요 누른 사람들 전부 확인

### 사용한 뮤테이션

- [1] login : 로그인 토큰을 받아옴
- [2] singup : 새로운 사용자를 만들고, 토큰을 받아옴
- [3] togglePhotoLike : 사진에 좋아요를 추가하거나 제거함
- [4] createComment : 사진에 댓글을 추가함
